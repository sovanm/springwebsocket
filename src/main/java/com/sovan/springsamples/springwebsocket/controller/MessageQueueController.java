package com.sovan.springsamples.springwebsocket.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.sovan.springsamples.springwebsocket.configuration.model.MessageDetails;

@Controller
public class MessageQueueController {

	@MessageMapping("/messagequeue")
	@SendTo("/topic/status")
	public MessageDetails send(MessageDetails message) throws Exception {
	    String time = new SimpleDateFormat("HH:mm").format(new Date());
	    return new MessageDetails("Sovan", "Hello World", time.toString());
	}
}
